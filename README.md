# SSCNET Install Guide on Ubuntu 16.04 LTS, Cuda 8 & Cudnn 6 

This repo contains a guide to compile and install  [SSCNet](http://sscnet.cs.princeton.edu/) on a very especific environment configuration.

Before you start, make shure Anaconda is not active in you session shell. If necessary, create a new user in you Ubuntu system (That's what I did). 

### Contents
0. [Requirements](#requirements)
0. [Organization](#organization)
0. [Compile & Install OpenCV 3.3](#opencv")
0. [Compile & Install SSCNET](#sscnet)
0. [Quick Demo](#quick-demo)
0. [Testing](#testing)
0. [Training](#training)
0. [Visualization and Evaluation](#visualization-and-evaluation)
0. [Data Preparation](#data)



### Organization
The project just include files required to ensure compatibility with cudnn 6. We to get full src files [here](https://github.com/shurans/sscnet)
``` shell
    sscnet_cudnn6
         |-- caffe_code
                    |-- caffe3d_suncg
                        |--include
                            |--caffe
                                |--util
```

### Requirements
1. Ubuntu 16.04 LTS
2. Cuda 8
3. Cudnn 6



### Compile & Install OpenCV 3.3 

Reference: https://github.com/BVLC/caffe/wiki/OpenCV-3.3-Installation-Guide-on-Ubuntu-16.04

1. Install OpenCV requirements:

    ```Shell
    sudo apt-get install --assume-yes build-essential cmake git
    sudo apt-get install --assume-yes pkg-config unzip ffmpeg qtbase5-dev python-dev python3-dev python-numpy python3-numpy
    sudo apt-get install --assume-yes libopencv-dev libgtk-3-dev libdc1394-22 libdc1394-22-dev libjpeg-dev libpng12-dev libtiff5-dev libjasper-dev
    sudo apt-get install --assume-yes libavcodec-dev libavformat-dev libswscale-dev libxine2-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev
    sudo apt-get install --assume-yes libv4l-dev libtbb-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev
    sudo apt-get install --assume-yes libvorbis-dev libxvidcore-dev v4l-utils vtk6
    sudo apt-get install --assume-yes liblapacke-dev libopenblas-dev libgdal-dev checkinstall
    ```

2. Download the latest source archive for OpenCV 3.3 from  https://github.com/opencv/opencv/archive/3.3.0.zip

3. Build & Install

    Enter the unpacked directory and execute:

    ```Shell
        mkdir build
        cd build/    
        cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D FORCE_VTK=ON -D WITH_TBB=ON -D WITH_V4L=ON -D WITH_QT=ON -D WITH_OPENGL=ON -D WITH_CUBLAS=ON -D CUDA_NVCC_FLAGS="-D_FORCE_INLINES" -D WITH_GDAL=ON -D WITH_XINE=ON -D BUILD_EXAMPLES=ON ..
        make -j $(($(nproc) + 1))
        
        sudo make install
        sudo /bin/bash -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf'
        sudo ldconfig
        sudo apt-get update
    ```
    

### Compile & Install SSCNET    

Reference: https://github.com/shurans/sscnet

0. Clone the repository

    ```Shell
    cd
    git clone https://github.com/shurans/sscnet.git
    ```
0. Cuda 6 compatibility

Adjusted files provided in this repo!

Make these changes in  include\caffe\util\cudnn.hpp:

    inline const char* cudnnGetErrorString(cudnnStatus_t status) {
      switch (status) {
      case CUDNN_STATUS_SUCCESS:
        return "CUDNN_STATUS_SUCCESS";
      case CUDNN_STATUS_NOT_INITIALIZED:
        return "CUDNN_STATUS_NOT_INITIALIZED";
      case CUDNN_STATUS_ALLOC_FAILED:
        return "CUDNN_STATUS_ALLOC_FAILED";
      case CUDNN_STATUS_BAD_PARAM:
        return "CUDNN_STATUS_BAD_PARAM";
      case CUDNN_STATUS_INTERNAL_ERROR:
        return "CUDNN_STATUS_INTERNAL_ERROR";
      case CUDNN_STATUS_INVALID_VALUE:
        return "CUDNN_STATUS_INVALID_VALUE";
      case CUDNN_STATUS_ARCH_MISMATCH:
        return "CUDNN_STATUS_ARCH_MISMATCH";
      case CUDNN_STATUS_MAPPING_ERROR:
        return "CUDNN_STATUS_MAPPING_ERROR";
      case CUDNN_STATUS_EXECUTION_FAILED:
        return "CUDNN_STATUS_EXECUTION_FAILED";
      case CUDNN_STATUS_NOT_SUPPORTED:
        return "CUDNN_STATUS_NOT_SUPPORTED";
      case CUDNN_STATUS_LICENSE_ERROR:
        return "CUDNN_STATUS_LICENSE_ERROR";
      case CUDNN_STATUS_RUNTIME_PREREQUISITE_MISSING:
        return "CUDNN_STATUS_RUNTIME_PREREQUISITE_MISSING";
      }
      return "Unknown cudnn status";
    }

    template <typename Dtype>
    inline void setConvolutionDesc(cudnnConvolutionDescriptor_t* conv,
                                cudnnTensorDescriptor_t bottom, cudnnFilterDescriptor_t filter,
                                int pad_h, int pad_w, int stride_h, int stride_w) {
                                CUDNN_CHECK(cudnnSetConvolution2dDescriptor_v4(*conv,
                                                 pad_h, pad_w, stride_h, stride_w, 1, 1, CUDNN_CROSS_CORRELATION));
                                }


0. Download the data: download_data.sh (1.1 G) *Updated on Sep 27 2017*
0. Download the pretrained models: download_models.sh (9.9M)
0. [optional] Download the training data: download_suncgTrain.sh (16 G)
0. [optional] Download the results: download_results.sh (8.2G)
0. Requirements for `Caffe` and `pycaffe` (see: [Caffe installation instructions](http://caffe.berkeleyvision.org/installation.html))

0. Compile & Install caffe and pycaffe with cmake. 
    1. Adjust CMakeList.txt
    
    ```Shell
        # ---[ Dependencies
        include(cmake/Dependencies.cmake)
        
        # ---[ Flags
        if(UNIX OR APPLE)
          set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC -Wall")
        endif()
        
        ##############################
        # included by Aloisio
        SET( CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -std=c++11" )
        ##############################
        
        caffe_set_caffe_link()
        
        if(USE_libstdcpp)
          set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libstdc++")
          message("-- Warning: forcing libstdc++ (controlled by USE_libstdcpp option in cmake)")
        endif()
    ```
 
    
    2. Compile  
    ```Shell
        mkdir build
        cd build
        cmake ..
        make all
        make install
        make runtest

        ```
    3. Export path

     ```Shell 
         export LD_LIBRARY_PATH=~/sscnet/caffe_code/caffe3d_sung/build/install/lib:$LD_LIBRARY_PATH
         export PYTHONPATH=~/sscnet/caffe_code/caffe3d_sung/build/install/python:$PYTHONPATH
     ```

### Quick Demo:
  ```Shell 
  cd demo
  python demotest_model.py
  ```
This demo runs semantic scene compeletion on one NYU depth map using our pretrained model and outputs a '.ply' visulization of the result.


### Testing:
0. Run the testing script
  ` cd caffe_code/script/test
    python test_model.py`
0. The output results will be stored in folder `results` in .hdf5 format
0. To test on other testsets (e.g. suncg, nyu, nyucad) you need to modify the paths in “test_model.py”.
    


### Training:
0. Finetuning on NYU 
    `cd caffe_code/train/ftnyu
      ./train.sh`
0. Training from scratch 
    ` cd caffe_code/train/trainsuncg
    ./train.sh`
0. To get more training data from SUNCG, please refer to the SUNCG toolbox 
    


### Visualization and Evaluation:
0. After testing, the results should be stored in folder `results/`
0. You can also download our precomputed results:
   `./download_results.sh`
0. Run the evaluation code in matlab:

    ``` shell 
    matlab &
    cd matlab_code
    evaluation_script('../results/','nyucad')
    ```
0. The visualization of results will be stored in `results/nyucad` as “.ply” files.



### Data 
0. Data format 
    1. Depth map : 
        16 bit png with bit shifting.
        Please refer to `./matlab_code/utils/readDepth.m` for more information about the depth format.
    2. 3D volume: 
        First three float stores the origin of the 3D volume in world coordinate.
        Then 16 float of camera pose in world coordinate.
        Followed by the 3D volume encoded by run-length encoding.
        Please refer to `./matlab_code/utils/readRLEfile.m` for more details.
0. Example code to convert NYU ground truth data: `matlab_code/perpareNYUCADdata.m` 
   This function provides an example of how to convert the NYU ground truth from 3D CAD model annotations provided by:
   Guo, Ruiqi, Chuhang Zou, and Derek Hoiem. "Predicting complete 3d models of indoor scenes."
   You need to download the original annotations by runing `download_UIUCCAD.sh`.  
0. Example code to generate testing data without ground truth and room boundary: `matlab_code/perpareDataTest.m`
   This function provides an example of how to generate your own testing data without ground truth labels. It will generate a the .bin file with camera pose and an empty volume, without room boundary. 
   

### Generating training data from SUNCG

You can generate more training data from SUNCG by following steps: 

0. Download SUNCG data and toolbox from: https://github.com/shurans/SUNCGtoolbox
0. Compile the toolbox.
0. Download the voxel data for objects (`download_objectvox.sh`) and move the folder under SUNCG data directory. 
0. Run the script: genSUNCGdataScript()
   You may need to modify the following paths:`suncgDataPath`, `SUNCGtoolboxPath`, `outputdir`.

 



### License
Code is released under the MIT License (refer to the LICENSE file for details).

    